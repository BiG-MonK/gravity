$(function(){

//------------------- services-carousel 
  $('.js-services-carousel').owlCarousel({
    loop: true,
    margin: 30,
    nav: true,
    dots: false,
    navText: ['<img src="img/slider-arrow-left.svg">', '<img src="img/slider-arrow-right.svg">'],
    responsive: 
    {
      0: { items: 1.3 },
      600: { items: 1.3 },
      1000: { items: 1.3 }
    }
  })

//------------------- testimonials__carousel
  $('.js-testimonials__carousel').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    dots: false,
    navText: ['<img src="img/slider-arrow-left.svg">', '<img src="img/slider-arrow-right.svg">'],
    responsive:
    {
      0: { items: 1 },
      600: { items: 1 },
      1000: { items:1 }
    }
  })

//------------------- works__filter
  var mixer = mixitup('.js-works__filter', {
    selectors: {
        target: '.works__item'
    },
    animation: {
      "duration": 1000,
      "nudge": true,
      "reverseOut": true,
      "effects": "fade scale(0.45)"
  }
});

  $('[data-filter]').on('click', function(elem) {
    elem.preventDefault()
  });

//------------------- input-search
  $('.js-nav__input-search')
    .focusin(function(){ $(this).animate({width: '82%', opacity: 1}, "fast")})
    .focusout(function(){ $(this).animate({width: 0, opacity: 0}, "fast")})

  $('.js-nav__search-ref').on('click', function(elem) {
    $('.nav__input-search').focus()
  })

//------------------- menu-burger
  $('.nav__menu').on('click', function(){
    let menu = $('.menu-burger');
    if (menu.hasClass('menu-burger--active')) {
      menu
      .animate({width: '0', height: '0', 'font-size': '0', opacity: '0'}, "fast")
      .removeClass('menu-burger--active')
    } else {
      menu
      .animate({width: '280', height: '400', 'font-size': '22', opacity: '1'}, "fast")
      .addClass('menu-burger--active');
    }
  })

  $(document).mouseup(function (e) { //close when pressed anywhere on the screen
    console.log(e)
    if ($('.nav__menu').has(e.target).length === 0){
      $('.menu-burger')
        .animate({width: '0', height: '0', 'font-size': '0', opacity: '0'}, "fast")
        .removeClass('menu-burger--active')
    }
  });

})

//------------------- slowScroll
function slowScroll(id) {
  let offset = 0;
  $('html, body').animate({
    scrollTop: $(id).offset().top - offset}, 500);
  return false;
}